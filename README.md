# flask-webservice

General web-service implementation in flask

### Generate database
run the following:

```python
from app import db
db.create_all()
```

to check the tables:

```sqlite3
.tables
.schema TABLE_NAME;
```
